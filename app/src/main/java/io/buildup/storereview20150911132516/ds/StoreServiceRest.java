/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ds;
import java.util.List;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.http.Path;
import retrofit.http.PUT;
import retrofit.http.Header;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.POST;
import retrofit.mime.TypedByteArray;
import retrofit.http.Part;
import retrofit.http.Multipart;

public interface StoreServiceRest{

	@GET("/api/stores")
	public void queryStoreItem(
		@Query("skip") String skip,
		@Query("limit") String limit,
		@Query("conditions") String conditions,
		@Query("sort") String sort,
		@Query("select") String select,
		@Query("populate") String populate,
		Callback<StoreItem.List> cb);

	@GET("/api/stores/{id}")
	public void getStoreItemById(@Path("id") String id, Callback<StoreItem> cb);

	@DELETE("/api/stores/{id}")
    public void deleteStoreItemById(@Path("id") String id, Callback<Object> cb);

    @POST("/api/stores/deleteByIds")
    public void deleteByIds(@Body List<String> ids, Callback<List<StoreItem>> cb);

    @POST("/api/stores")
    public void createStoreItem(@Body StoreItem item, Callback<StoreItem> cb);

    @PUT("/api/stores/{id}")
    public void updateStoreItem(@Path("id") String id, @Body StoreItem item, Callback<StoreItem> cb);

    @GET("/api/stores")
    public void distinct(@Query("distinct") String colName, Callback<List<String>> cb);
    
    @Multipart
    @POST("/api/stores")
    public void createStoreItem(
        @Part("data") StoreItem item,
        @Part("picture") TypedByteArray picture,
        Callback<StoreItem> cb);
    
    @Multipart
    @PUT("/api/stores/{id}")
    public void updateStoreItem(
        @Path("id") String id,
        @Part("data") StoreItem item,
        @Part("picture") TypedByteArray picture,
        Callback<StoreItem> cb);
}
