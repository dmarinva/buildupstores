/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.interactors;
import io.buildup.storereview20150911132516.ds.StoreServiceRest;
import io.buildup.storereview20150911132516.ds.StoreItem;
import io.buildup.storereview20150911132516.events.DeleteStoreItemResult;

import buildup.ds.RestService;
import buildup.events.Bus;
import buildup.events.DatasourceFailureEvent;
import buildup.events.Job;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * StoreItem Delete Job
 */
public class DeleteStoreItemJob implements Job{
 	private final StoreItem mItem;
    private final RestService<StoreServiceRest> mService;
    private final Bus mBus;

    public DeleteStoreItemJob(StoreItem item,
                         RestService<StoreServiceRest> service,
                         Bus bus){
        super();
        mService = service;
        mItem = item;
        mBus = bus;
    }

    @Override
    public void onRun() {
        mService.getServiceProxy().deleteStoreItemById(mItem.id, new Callback<Object>() {
            @Override
            public void success(Object result, Response response) {
                mBus.post(new DeleteStoreItemResult());
            }

            @Override
            public void failure(RetrofitError error) {
                mBus.post(new DatasourceFailureEvent());
            }
        });
    }
}
