/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.mvp;

import java.util.List;

public interface ListCrudPresenter<T> extends Presenter{
    /**
     * delete one item
     * @param item
     */
    void deleteItem(T item);

    /**
     * delete a list of items
     * @param items
     */
    void deleteItems(List<T> items);

    /**
     * Go to create form
     */
    void addForm();

    /**
     * Go to edit form
     * @param item
     */
    void editForm(T item);

    /**
     * Go to detail
     * @param item
     */
    void detail(T item);
}
