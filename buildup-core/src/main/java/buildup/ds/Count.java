/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ds;

/**
 * Use this interface to mark datasources as "countable"
 */
public interface Count {

    /**
     * Get the size of this datasource
     *
     * @return the number of elements that this datasource provides
     */
    public int getCount();
}
