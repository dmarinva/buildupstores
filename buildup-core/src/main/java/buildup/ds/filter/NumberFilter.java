package buildup.ds.filter;

/**
 * Created by jiparis on 15/09/2015.
 */
public class NumberFilter implements Filter<Number> {

    private final Number mValue;
    private final String mField;

    public NumberFilter(String field, Number value){
        mField = field;
        mValue = value;
    }

    @Override
    public String getField() {
        return mField;
    }

    @Override
    public String getQueryString() {
        return "\"" + mField + "\":{\"$eq\":" + mValue + "}";
    }

    @Override
    public boolean applyFilter(Number fieldValue) {
        return (mValue == null) || mValue.equals(fieldValue);
    }
}
