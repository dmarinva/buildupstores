/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.mvp;

import android.util.SparseArray;

import buildup.R;
import buildup.ds.RestService;
import buildup.events.Bus;
import buildup.events.DatasourceFailureEvent;
import buildup.events.Executor;
import buildup.validation.Validator;

public abstract class BaseFormPresenter<T> extends BasePresenter implements FormPresenter<T>{
    private SparseArray<Validator<T>> mValidators;

    final protected RestService mService;
    final protected Executor mExecutor;
    final protected FormView<T> mView;

    public BaseFormPresenter(RestService service, Bus bus, Executor executor, FormView<T> view){
        super(bus);
        mService = service;
        mExecutor = executor;
        mView = view;
    }

    protected RestService getService(){
        return mService;
    }

    protected Executor getExecutor(){
        return mExecutor;
    }

    protected FormView<T> getView(){
        return mView;
    }

    @Override
    public void cancel() {
        mView.close(false);
    }

    @Override
    public void addValidator(int viewId, Validator<T> validator){
        if(this.mValidators == null)
            this.mValidators = new SparseArray<Validator<T>>();

        this.mValidators.put(viewId, validator);
    }

    @Override
    public boolean validate(T item){
        boolean res = true;
        if(mValidators != null) {
            for (int i = 0; i < mValidators.size(); i++) {
                Validator<T> val = mValidators.get(mValidators.keyAt(i));
                if (!val.validate(item)) {
                    val.setError(true);
                    res = false;
                } else
                    val.setError(false);
            }
        }

        return res;
    }

    public void onEvent(DatasourceFailureEvent ev){
        // Generic error event
        if(getView() != null)
            getView().showMessage(R.string.error_data_generic, false);
    }

}
