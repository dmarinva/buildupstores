/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.validation;

public interface Validator<T> {
    public boolean validate(T item);
    public void setError(boolean show);
}
