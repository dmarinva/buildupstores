package buildup.maps.presenters;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import buildup.ds.Datasource;
import buildup.ds.SearchOptions;
import buildup.ds.filter.Filter;
import buildup.ds.restds.GeoPoint;
import buildup.events.Bus;
import buildup.events.Executor;
import buildup.maps.events.MapDataReadyEvent;
import buildup.maps.interactors.FetchMapPointsJob;
import buildup.mvp.BasePresenter;

public class MapPresenter<T> extends BasePresenter {

    private final MapView<T> mView;
    private final Datasource<T> mDatasource;
    private final Executor mExecutor;
    private final String mField;

    public static final int MODE_NORMAL = 0;
    public static final int MODE_SEARCH = 1;

    private int mMode = MODE_NORMAL;
    private SearchOptions mSearchOptions = new SearchOptions();

    public MapPresenter(Bus bus, Executor ex, Datasource<T> datasource, String field, MapView<T> view) {
        super(bus);
        mView = view;
        mDatasource = datasource;
        mExecutor = ex;
        mField = field;
    }

    public void queryMapData(GeoPoint center, long distance) {
        // if we are searching, then deactivate distance query
        long realdistance = mSearchOptions.getSearchText() != null ? 0 : distance;
        mExecutor.execute(new FetchMapPointsJob<T>(mDatasource, mBus, mField,
                center, realdistance, mSearchOptions));
    }

    public void queryMapData(GeoPoint sw, GeoPoint ne){
        String st = mSearchOptions.getSearchText();
        mExecutor.execute(new FetchMapPointsJob<T>(mDatasource, mBus, mField,
                st != null ? null : sw,
                st != null ? null : ne,
                mSearchOptions));
    }

    public void onEvent(MapDataReadyEvent<T> ev){
        mView.bindMapData(ev.data);
        if(mMode == MODE_SEARCH && ev.data.size() > 0)
            mView.revealItem(ev.data.get(0));
    }

    public void setTextFilter(String textFilter) {
        if(textFilter != null && textFilter.length() > 0)
            setMode(MODE_SEARCH);
        else
            setMode(MODE_NORMAL);

        mSearchOptions.setSearchText(textFilter);
    }

    public void cameraChange(GeoPoint mCenter, long distance) {
        if(mMode == MODE_NORMAL)
            queryMapData(mCenter, distance);
    }

    public void cameraChange(GeoPoint sw, GeoPoint ne){
        if(mMode == MODE_NORMAL)
            queryMapData(sw, ne);
    }

    // Sets a filter that will be applied to all jobs queries
    public void addFixedFilter(Filter filter) {
        mSearchOptions.addFixedFilter(filter);
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({MODE_NORMAL, MODE_SEARCH})
    public @interface MapMode {}
    public void setMode(@MapMode int mode){
        mMode = mode;
    }

    public interface MapView<T>{
        void bindMapData(List<T> data);
        void navigateToDetail(T item);
        void revealItem(T item);
    }
}
