/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.behaviors;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import buildup.R;
import buildup.ui.Refreshable;

/**
 * Add refresh pattern to lists
 */
public class RefreshBehavior extends AbstractBehavior {

    private Refreshable mFragment;

    public RefreshBehavior(Refreshable fragment) {
        mFragment = fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.refresh_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            mFragment.refresh();
            return true;
        }
        return false;
    }
}
