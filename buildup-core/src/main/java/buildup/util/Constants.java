/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.util;

public interface Constants {

    final String ITEMPOS = "itempos";
    final String TITLE = "title";
    final String SIZE = "size";
    final String CONTENT = "content";
    final String MODE = "mode";
    int MODE_EDIT = 1;
    int MODE_CREATE = 2;
    int DETAIL = 3;

    final int CONTENT_UPDATED = 100;
    final int CONTENT_NOT_UPDATED = 101;
}
