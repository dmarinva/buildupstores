/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */


package io.buildup.storereview20150911132516.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import buildup.ui.DrawerActivity;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcelable;

import buildup.actions.StartActivityAction;
import buildup.events.ItemEditEvent;
import buildup.events.ItemSelectedEvent;
import buildup.util.Constants;
import io.buildup.storereview20150911132516.ds.StoreItem;

public class DavidTestMain extends DrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Class<? extends Fragment>[] getSectionFragmentClasses() {
        return new Class[]{
                StoresFragment.class,
                RatingsFragment.class,
                InspectionsFragment.class,
                SalesChartFragment.class
        };
    }

    @Override
    public String[] getSectionTitles() {
        return new String[]{
                "Stores",
                "Ratings",
                "Inspections",
                "SalesChart"
        };
    }

    //List of navigation actions
    @Override
    public List<String> getNavigationActions() {
        return new ArrayList<>();
    }

    @Override
    public void callAction(int i) {

    }

    // Events
    public void onEvent(ItemSelectedEvent ev) {
        if (ev.target instanceof StoreItem && "InspectionsFragment".equals(ev.tag)) {
            StoreItem item = (StoreItem) ev.target;

            Bundle args = new Bundle();

            args.putInt(Constants.ITEMPOS, ev.position);
            args.putParcelable(Constants.CONTENT, item);
            new StartActivityAction(this, InspectionsDetailActivity.class, Constants.DETAIL).execute(args);
        }
    }

    public void onEvent(ItemEditEvent ev) {

        if (ev.tag.equals(StoreItem.class)) {
            Bundle args = new Bundle();

            args.putInt(Constants.ITEMPOS, ev.position);
            args.putParcelable(Constants.CONTENT, (Parcelable) ev.target);
            args.putInt(Constants.MODE, ev.target == null ? Constants.MODE_CREATE : Constants.MODE_EDIT);
            new StartActivityAction(this, StoreItemFormActivity.class, Constants.MODE_EDIT).execute(args);
        }
    }
}
