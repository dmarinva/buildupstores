/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ds;

import java.util.List;

/**
 * Public interface for Asynchronous datasource
 */
public interface Datasource<T> {

    /**
     * Get all items
     *
     * @param listener the callback to call when this operation has finished
     */
    public void getItems(Listener<List<T>> listener);

    /**
     * Get a concrete item
     *
     * @param id       the id of the item in the datasource
     * @param listener the callback to call when this operation has finished
     */
    public void getItem(String id, Listener<T> listener);

    /**
     * Public interface for datasource operation callbacks
     *
     * @param <RESULT> the type of the results
     */
    public interface Listener<RESULT> {

        /**
         * Called on succesful operations
         *
         * @param result the result of the operation
         */
        public void onSuccess(RESULT result);

        /**
         * Called when something has failed
         *
         * @param e the exception details
         */
        public void onFailure(Exception e);
    }
}
