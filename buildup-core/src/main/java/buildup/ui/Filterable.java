/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ui;

import buildup.ds.filter.Filter;

/**
 * UI component (activity or fragment) that supports search operations
 */
public interface Filterable extends Refreshable {

    /**
     * Set the search parameter
     */
    public void setSearchText(String s);

    /**
     * filters
     * @param filter
     */
    public void addFilter(Filter filter);

    public void addFixedFilter(Filter filter);
}
