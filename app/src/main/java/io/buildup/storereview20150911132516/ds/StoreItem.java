/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */


package io.buildup.storereview20150911132516.ds;
import java.util.Date;
import android.graphics.Bitmap;
import buildup.ds.restds.GeoPoint;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class StoreItem implements Parcelable{

    public String name;
    public Long sales;
    public String lastReview;
    public String review;
    public Date reviewDate;
    public Long performance;
    public String picture;
    public transient Bitmap pictureBitmap;
    @SerializedName("_id") public String id;
    public GeoPoint location = new GeoPoint();

    // Parcelable interface

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeValue(sales);
        dest.writeString(lastReview);
        dest.writeString(review);
        dest.writeValue(reviewDate != null ? reviewDate.getTime() : null);
        dest.writeValue(performance);
        dest.writeString(picture);
        dest.writeString(id);
        dest.writeDoubleArray(location != null ? location.coordinates : null);
    }

    public static final Creator<StoreItem> CREATOR = new Creator<StoreItem>() {
        @Override
        public StoreItem createFromParcel(Parcel in) {
            StoreItem item = new StoreItem();

            item.name = in.readString();
            item.sales = (Long) in.readValue(null);
            item.lastReview = in.readString();
            item.review = in.readString();
            Long reviewDateAux = (Long) in.readValue(null);
            item.reviewDate = reviewDateAux != null ? new Date(reviewDateAux) : null;
            item.performance = (Long) in.readValue(null);
            item.picture = in.readString();
            item.id = in.readString();
            double[] location_coords = in.createDoubleArray();
            if (location_coords != null)
                item.location = new GeoPoint(location_coords);
           
            return item; 
        }

        @Override
        public StoreItem[] newArray(int size) {
            return new StoreItem[size];
        }
    };

    public static class List extends ArrayList<StoreItem>{}

}

