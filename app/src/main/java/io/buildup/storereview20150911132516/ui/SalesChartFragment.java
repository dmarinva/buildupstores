package io.buildup.storereview20150911132516.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

import buildup.ds.Datasource;
import io.buildup.storereview20150911132516.R;
import io.buildup.storereview20150911132516.ds.StoreDSDS;
import io.buildup.storereview20150911132516.ds.StoreItem;

public class SalesChartFragment extends Fragment implements Datasource.Listener<List<StoreItem>> {

    private BarChart barChart;

    public static SalesChartFragment newInstance(Bundle args) {
        return new SalesChartFragment();
    }


        @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sales_chart, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        barChart = (BarChart) view.findViewById(R.id.bar_chart);
        barChart.setPinchZoom(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setSpaceBetweenLabels(2);

        StoreDSDS storesDataSource = StoreDSDS.getInstance(getActivity().getApplicationContext());
        storesDataSource.getItems(this);

    }

    @Override
    public void onSuccess(List<StoreItem> storeItems) {
        if (storeItems == null) {
            onFailure(new IllegalArgumentException("No stores found"));
        } else {
            generateData(storeItems);
        }
    }

    @Override
    public void onFailure(Exception e) {
        Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
    }

    private void generateData(@NonNull List<StoreItem> storeItems) {
        List<String> stores = new ArrayList<>();
        List<BarEntry> entries = new ArrayList<>();
        int i = 0;
        for (StoreItem storeItem : storeItems) {
            //Why do we have stores without name?
            //TODO filter values while doing the query instead of here.
            if (storeItem.name != null) {
                stores.add(storeItem.name);
                Long sales = storeItem.sales;
                entries.add(new BarEntry(sales == null ? 0 : sales, i));
                i++;
            }
        }
        BarDataSet barDataSet = new BarDataSet(entries, getString(R.string.sales));
        BarData barData = new BarData(stores, barDataSet);
        barChart.setData(barData);
        barChart.invalidate();
    }
}
