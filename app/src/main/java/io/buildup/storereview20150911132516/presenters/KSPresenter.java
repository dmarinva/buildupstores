/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.presenters;

import buildup.events.Bus;
import buildup.events.DummyEvent;
import buildup.events.DummyJob;
import buildup.events.Executor;
import buildup.mvp.BasePresenter;

public class KSPresenter extends BasePresenter {

    private final KSView mView;
    private final Executor mExecutor;
    private final long mSplashDuration;
    private final long mFirstOpen;
    private final long mKsDuration;

    public KSPresenter(Bus bus, Executor executor, KSView view, long firstOpen, long splashDuration, long ksDuration) {
        super(bus);

        mView = view;
        mExecutor = executor;

        mFirstOpen = firstOpen;
        mSplashDuration = splashDuration;
        mKsDuration = ksDuration;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!ksExpired()){
            countDownAndContinue();
        }
        else{
            mView.showAppExpired();
        }
    }

    private boolean ksExpired(){
        long current = System.currentTimeMillis();

        if(mFirstOpen + mKsDuration < current)
            return true;

        return false;
    }

    private void countDownAndContinue(){
        mExecutor.executeDelayed(new DummyJob(), mSplashDuration);
    }

    // dummy event
    public void onEvent(DummyEvent ev){
        mView.startApp();
    }

    public static interface KSView{
        void startApp();
        void showAppExpired();
    }
}

