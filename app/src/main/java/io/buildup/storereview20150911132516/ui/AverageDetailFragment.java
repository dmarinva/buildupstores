/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ui;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;
import buildup.behaviors.ShareBehavior;
import io.buildup.storereview20150911132516.R;
import buildup.ds.Datasource;
import io.buildup.storereview20150911132516.ds.StoreDSDS;
import io.buildup.storereview20150911132516.ds.StoreItem;

public class AverageDetailFragment extends buildup.ui.DetailFragment<StoreItem> implements ShareBehavior.ShareListener {

    public static AverageDetailFragment newInstance(Bundle args){
        AverageDetailFragment fr = new AverageDetailFragment();
        fr.setArguments(args);

        return fr;
    }

    public AverageDetailFragment(){
        super();
    }      

    @Override
    public Datasource<StoreItem> getDatasource(){
        return StoreDSDS.getInstance(getActivity().getApplication());
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        addBehavior(new ShareBehavior(getActivity(), this));
    }

    // Bindings

    @Override
    protected int getLayout() {
        return R.layout.averagedetail_detail;
    }

    @Override
    @SuppressLint("WrongViewCast")
    public void bindView(final StoreItem item, View view) {
        if (item.lastReview != null){
            
            TextView view0 = (TextView) view.findViewById(R.id.view0); 
            view0.setText("\"" + item.lastReview + "\"");
            
        }
        if (item.reviewDate != null){
            
            TextView view1 = (TextView) view.findViewById(R.id.view1); 
            view1.setText(DateFormat.getMediumDateFormat(getActivity()).format(item.reviewDate));
            
        }
        if (item.performance != null){
            
            TextView view2 = (TextView) view.findViewById(R.id.view2); 
            view2.setText("EVAL = " + item.performance.toString());
            
        }
        
        TextView view3 = (TextView) view.findViewById(R.id.view3); 
        view3.setText(" (1 is high, 3 is low)");
        
        if (item.review != null){
            
            TextView view4 = (TextView) view.findViewById(R.id.view4); 
            view4.setText("\"" + item.review + "\"");
            
        }
    }

    @Override
    protected void onShow(StoreItem item) {
        // set the title for this fragment
        getActivity().setTitle(null);
    }
        
    @Override
    public void onShare() {
        StoreItem item = getItem();
           
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");

        intent.putExtra(Intent.EXTRA_TEXT, (item.lastReview != null ? "\"" + item.lastReview + "\"" : "" ) + "\n" +
                    (item.reviewDate != null ? DateFormat.getMediumDateFormat(getActivity()).format(item.reviewDate) : "" ) + "\n" +
                    (item.performance != null ? "EVAL = " + item.performance.toString() : "" ) + "\n" +
                    " (1 is high, 3 is low)" + "\n" +
                    (item.review != null ? "\"" + item.review + "\"" : "" ));
        startActivityForResult(Intent.createChooser(intent,
                        getString(R.string.share)), 1);
    }    
}
