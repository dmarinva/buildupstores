/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.events;

/**
 * bus event
 */
public class ItemSelectedEvent {

    public int position;

    public Object target;

    public String tag;

    public ItemSelectedEvent(int position, String tag, Object target) {
        this.position = position;
        this.target = target;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return new StringBuilder("ItemSelectedEvent: ")
                .append("(position = ")
                .append(position)
                .append(", obj = ")
                .append(target.toString())
                .append(")")
                .toString();
    }
}
