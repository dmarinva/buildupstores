/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.actions;


import android.os.Bundle;

public interface Action {

    public void execute(Bundle runtimeArgs);
}
