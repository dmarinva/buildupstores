/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.events;

/**
 * Job that does nothing. Usually for signaling delayed events.
 */
public class DummyJob implements Job {

    @Override
    public void onRun() {
        BusProvider.getInstance().post(new DummyEvent());
    }
}