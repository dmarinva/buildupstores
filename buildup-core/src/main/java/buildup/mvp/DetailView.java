/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.mvp;

/**
 * View (as in MVP) for details
 */
public interface DetailView {
    public void refresh();
    public void close(boolean shouldRefresh);
    public void showMessage(int message, boolean toast);
    public void navigateToEditForm();
}
