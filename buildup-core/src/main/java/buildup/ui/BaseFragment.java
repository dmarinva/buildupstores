/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import buildup.actions.Action;
import buildup.behaviors.Behavior;
import buildup.events.BusProvider;
import buildup.mvp.Presenter;

/**
 * Base fragment with common support code (bus registration, etc)
 */
public class BaseFragment extends Fragment {

    protected List<Behavior> mBehaviors;
    protected Presenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // we want to add buttons to the actionbar
        setHasOptionsMenu(true);
    }

    // fragment lifecygle

    @Override
    public void onResume() {
        super.onResume();
        // TODO: Remove unregister from here (it's presenter responsibility)
        BusProvider.getInstance().register(this);
        if(mPresenter != null){
            mPresenter.onResume();
        }

        // intialize behaviors
        if (mBehaviors != null) {
            for (Behavior b : mBehaviors) {
                b.resume();
            }
        }
    }

    @Override
    public void onPause() {
        super.onResume();
        // TODO: Remove unregister from here (it's presenter responsibility)
        BusProvider.getInstance().unregister(this);
        if(mPresenter != null){
            mPresenter.onPause();
        }
        // pause behaviors
        if (mBehaviors != null) {
            for (Behavior b : mBehaviors) {
                b.pause();
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mBehaviors != null) {
            for (Behavior b : mBehaviors) {
                b.onViewCreated(view, savedInstanceState);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mBehaviors != null) {
            for (Behavior b : mBehaviors) {
                b.onCreateOptionsMenu(menu, inflater);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean managed = false;
        if (mBehaviors != null) {
            for (Behavior b : mBehaviors) {
                managed = managed || b.onOptionsItemSelected(item);
            }
        }

        return managed || super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(mBehaviors != null){
            for (Behavior b : mBehaviors){
                b.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // clear resources
        if (mBehaviors != null) {
            mBehaviors.clear();
        }
        mBehaviors = null;
    }

    /**
     * Adds a {@link Behavior} to this fragment
     *
     * @param behavior the behavior to add to this fragment
     */
    public void addBehavior(Behavior behavior) {
        if (mBehaviors == null) {
            mBehaviors = new ArrayList<Behavior>();
        }

        this.mBehaviors.add(behavior);
    }

    /**
     * Sets the {@link Presenter} instance for this fragment view
     * This is not mandatory, but calling this method will sync fragment and presenter life cycles
     * @param presenter the presenter for this view
     */
    public void setPresenter(Presenter presenter){
        this.mPresenter = presenter;
    }

    /**
     * Returns the {@link Presenter} configured for this view
     * @return
     */
    public Presenter getPresenter(){
        return this.mPresenter;
    }

    /**
     * Binds a view to an action, so that the action is executed on click event
     *
     * @param view   the view to bind the action to
     * @param action the action to bind to the view
     */
    protected void bindAction(View view, final Action action) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action.execute(null);
            }
        });
    }

    /**
     * Generic event handler
     */
    public void onEvent(Object o){
        // noop
    }
}
