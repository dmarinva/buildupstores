/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.presenters;

import io.buildup.storereview20150911132516.R;
import io.buildup.storereview20150911132516.ds.StoreItem;
import io.buildup.storereview20150911132516.interactors.DeleteStoreItemJob;
import io.buildup.storereview20150911132516.events.DeleteStoreItemResult;

import buildup.ds.RestService;
import buildup.events.Bus;
import buildup.events.Executor;
import buildup.mvp.BasePresenter;
import buildup.mvp.DetailCrudPresenter;
import buildup.mvp.DetailView;

public class InspectionsDetailPresenter extends BasePresenter implements DetailCrudPresenter<StoreItem> {
    private final RestService mService;
    private final Executor mExecutor;
    private final DetailView mView;

    public InspectionsDetailPresenter(RestService service, Bus bus, Executor executor, DetailView view){
        super(bus);
        mService = service;
        mExecutor = executor;
        mView = view;
    }

    @Override
    public void deleteItem(StoreItem item) {
        mExecutor.execute(new DeleteStoreItemJob(item, mService, mBus));
    }

    public void onEvent(DeleteStoreItemResult result){
        mView.showMessage(R.string.item_deleted, true);
        mView.close(true);
    }
    
    @Override
    public void editForm(StoreItem item) {
        mView.navigateToEditForm();
    }   
}
