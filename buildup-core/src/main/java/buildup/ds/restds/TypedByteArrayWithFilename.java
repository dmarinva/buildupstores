/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ds.restds;

import retrofit.mime.TypedByteArray;

public class TypedByteArrayWithFilename extends TypedByteArray {

    private String fileName;

    public TypedByteArrayWithFilename(String mimeType, byte[] bytes, String fileName) {
        super(mimeType, bytes);
        this.fileName = fileName;
    }

    @Override public String fileName() {
        return fileName;
    }
}