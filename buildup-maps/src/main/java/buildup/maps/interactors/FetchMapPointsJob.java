package buildup.maps.interactors;

import java.util.List;

import buildup.ds.Datasource;
import buildup.ds.SearchOptions;
import buildup.ds.restds.AppNowDatasource;
import buildup.ds.restds.GeoPoint;
import buildup.events.Bus;
import buildup.events.DatasourceFailureEvent;
import buildup.events.Job;
import buildup.maps.ds.GeoFilter;
import buildup.maps.events.MapDataReadyEvent;

public class FetchMapPointsJob<T> implements Job {

    private SearchOptions mSearchOptions;
    private Datasource<T> mDatasource;
    private String mField;
    private GeoPoint mCenter;
    private long mDistance;
    private Bus mBus;
    private GeoPoint mSW;
    private GeoPoint mNE;

    public FetchMapPointsJob(Datasource<T> ds,
                             Bus bus,
                             String field,
                             GeoPoint center,
                             long distance,
                             SearchOptions searchOptions){
        mDatasource = ds;
        mField = field;
        mCenter = center;
        mDistance = distance;
        mBus = bus;
        mSearchOptions = searchOptions;
    }

    public FetchMapPointsJob(Datasource<T> ds,
                             Bus bus,
                             String field,
                             GeoPoint sw,
                             GeoPoint ne,
                             SearchOptions searchOptions){
        mDatasource = ds;
        mField = field;
        mSW = sw;
        mNE = ne;
        mBus = bus;
        mSearchOptions = searchOptions;
    }

    @Override
    public void onRun() {
        if(mDatasource instanceof AppNowDatasource){
            AppNowDatasource<T> ands = (AppNowDatasource<T>) mDatasource;

            if(mSearchOptions == null)
                mSearchOptions = new SearchOptions();

            mSearchOptions.addFilter(mCenter != null ? new GeoFilter(mField, mCenter, mDistance) :
                    new GeoFilter(mField, mSW, mNE));

            ands.getItems(mSearchOptions, new Datasource.Listener<List<T>>() {
                @Override
                public void onSuccess(List<T> ts) {
                    mBus.post(new MapDataReadyEvent<T>(ts));
                }

                @Override
                public void onFailure(Exception e) {
                    mBus.post(new DatasourceFailureEvent());
                }
            });
        }
    }
}
