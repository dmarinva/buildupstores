/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.Date;

import buildup.ds.restds.GeoPoint;
import buildup.events.BusProvider;
import buildup.events.ExecutorProvider;
import buildup.ui.FormFragment;
import buildup.util.StringUtils;
import buildup.validation.GeoPointValidator;
import buildup.views.DatePicker;
import buildup.views.GeoPicker;
import buildup.views.ImagePicker;
import buildup.views.TextWatcherAdapter;
import io.buildup.storereview20150911132516.R;
import io.buildup.storereview20150911132516.ds.StoreItem;
import io.buildup.storereview20150911132516.ds.StoreService;
import io.buildup.storereview20150911132516.presenters.InspectionsFormPresenter;

public class StoreItemFormFragment extends FormFragment<StoreItem> {

    public static StoreItemFormFragment newInstance(Bundle args) {
        StoreItemFormFragment fr = new StoreItemFormFragment();
        fr.setArguments(args);

        return fr;
    }

    public StoreItemFormFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        // the presenter for this view
        setPresenter(new InspectionsFormPresenter(
                getRestService(),
                BusProvider.getInstance(),
                ExecutorProvider.getInstance(getActivity().getApplication()),
                this));
    }

    @Override
    protected StoreItem newItem() {
        return new StoreItem();
    }

    private StoreService getRestService() {
        return StoreService.getInstance(getActivity().getApplication());
    }

    // Bindings

    @Override
    protected int getLayout() {
        return R.layout.inspections_form;
    }

    @Override
    @SuppressLint("WrongViewCast")
    public void bindView(final StoreItem item, View view) {

        bindString(R.id.store_name, item.name, new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                item.name = s.toString();
            }
        });


        bindLong(R.id.store_sales, item.sales, new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                item.sales = StringUtils.parseLong(s.toString());
            }
        });


        bindString(R.id.store_lastreview, item.lastReview, new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                item.lastReview = s.toString();
            }
        });


        bindString(R.id.store_review, item.review, new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                item.review = s.toString();
            }
        });


        bindDatePicker(R.id.store_reviewdate, item.reviewDate, new DatePicker.DateSelectedListener() {
            @Override
            public void onSelected(Date selected) {
                item.reviewDate = selected;
            }
        });

        bindProgress(R.id.store_performance, item.performance, new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                item.performance = Long.valueOf(progress);
                bindProgressTitle(R.id.performance_title, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bindImage(R.id.store_picture,
                item.picture != null ?
                        getRestService().getImageUrl(item.picture) : null,
                0,
                new ImagePicker.Callback() {
                    @Override
                    public void imageRemoved() {
                        item.picture = null;
                        item.pictureBitmap = null;
                        ((ImagePicker) getView().findViewById(R.id.store_picture)).clear();
                    }
                }
        );


        bindLocation(R.id.store_location, item.location,
                new GeoPicker.PointChangedListener() {
                    @Override
                    public void onPointChanged(GeoPoint point) {
                        item.location = point;
                    }
                },
                new GeoPointValidator<StoreItem>(getView(),
                        R.id.store_location,
                        R.string.error_field_required) {

                    @Override
                    public boolean validate(StoreItem item) {
                        return item.location != null;
                    }
                }
        );

    }

    private void bindProgress(int viewId, Long data, SeekBar.OnSeekBarChangeListener onSeekBarChangeListener) {
        SeekBar seekBar = (SeekBar) getView().findViewById(viewId);
        seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
        int progress = data == null ? 0 : data.intValue();
        seekBar.setProgress(progress);
        bindProgressTitle(R.id.performance_title, progress);
    }

    private void bindProgressTitle(int titleViewId, int progress) {
        TextView titleText = (TextView) getView().findViewById(titleViewId);
        titleText.setText(getString(R.string.performance, progress));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            ImagePicker picker = null;
            StoreItem item = getItem();

            if ((requestCode & ImagePicker.GALLERY_REQUEST_CODE) == ImagePicker.GALLERY_REQUEST_CODE) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(
                            getActivity().getContentResolver(), data.getData());

                    switch (requestCode - ImagePicker.GALLERY_REQUEST_CODE) {

                        case 0:   // picture field
                            item.pictureBitmap = bitmap;
                            item.picture = "cid:picture";
                            picker = (ImagePicker) getView().findViewById(R.id.store_picture);
                            break;


                    }

                    picker.setImageBitmap(bitmap);
                } catch (IOException e) {
                    // do nothing
                }

            } else if ((requestCode & ImagePicker.CAPTURE_REQUEST_CODE) == ImagePicker.CAPTURE_REQUEST_CODE) {
                Bitmap bitmap = data.getParcelableExtra("data");

                if (bitmap != null) {
                    switch (requestCode - ImagePicker.CAPTURE_REQUEST_CODE) {

                        case 0:   // picture field
                            item.pictureBitmap = bitmap;
                            item.picture = "cid:picture";
                            picker = (ImagePicker) getView().findViewById(R.id.store_picture);
                            break;


                    }

                    picker.setImageBitmap(bitmap);
                }
            }
        }
    }
}