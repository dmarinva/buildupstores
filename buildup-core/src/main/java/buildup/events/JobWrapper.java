/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.events;

import com.path.android.jobqueue.Params;

/**
 * Wrapps a Job into a JobQueue's Job
 */
public abstract class JobWrapper extends com.path.android.jobqueue.Job implements Job{

    protected JobWrapper() {
        this(0);
    }

    protected JobWrapper(long delay){
        super(new Params(0).delayInMs(delay));
    }

    @Override
    public void onAdded() {}

    @Override
    protected void onCancel() {}

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }

    @Override
    public abstract void onRun();
}
