/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.presenters;

import io.buildup.storereview20150911132516.R;
import io.buildup.storereview20150911132516.ds.StoreItem;
import io.buildup.storereview20150911132516.interactors.DeleteStoreItemJob;
import io.buildup.storereview20150911132516.interactors.DeleteStoreItemsJob;
import io.buildup.storereview20150911132516.events.DeleteStoreItemResult;

import java.util.List;

import buildup.ds.RestService;
import buildup.events.Bus;
import buildup.events.Executor;
import buildup.events.ItemEditEvent;
import buildup.mvp.BasePresenter;
import buildup.mvp.ListCrudPresenter;
import buildup.mvp.ListView;

public class InspectionsPresenter extends BasePresenter implements ListCrudPresenter<StoreItem> {
    private final RestService mService;
    private final Executor mExecutor;
    private final ListView mView;

    public InspectionsPresenter(RestService service, Bus bus, Executor executor, ListView view){
        super(bus);
        mService = service;
        mExecutor = executor;
        mView = view;
    }

    @Override
    public void deleteItem(StoreItem item) {
        mExecutor.execute(new DeleteStoreItemJob(item, mService, mBus));
    }

    @Override
    public void deleteItems(List<StoreItem> items) {
        // Create command and execute it
        mExecutor.execute(new DeleteStoreItemsJob(items, mService, mBus));
    }

    public void onEvent(DeleteStoreItemResult result){
        mView.showMessage(R.string.items_deleted);
        mView.refresh();
    }

    @Override
    public void addForm() {
        mBus.post(new ItemEditEvent(0, StoreItem.class, null));
    }

    @Override
    public void editForm(StoreItem item) {
        mBus.post(new ItemEditEvent(0, StoreItem.class, item));
    }

    @Override
    public void detail(StoreItem item) {
        mView.navigateToDetail(item);
    }

}

