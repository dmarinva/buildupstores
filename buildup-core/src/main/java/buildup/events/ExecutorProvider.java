/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.events;

import android.content.Context;

import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.config.Configuration;

/**
 * Executor based on Path's job queue implementation
 */
public class ExecutorProvider implements Executor{

    private static ExecutorProvider mInstance;
    private JobManager mJobManager;

    /**
     * Singleton method
     * @param context the application context
     * @return the Executor singleton
     */
    public static Executor getInstance(Context context){
        if(mInstance == null)
            mInstance = new ExecutorProvider(context);

        return mInstance;
    }

    public ExecutorProvider(Context context){
        Configuration config = new Configuration.Builder(context)
                .minConsumerCount(1)    // always keep at least one consumer alive
                .maxConsumerCount(3)    // up to 3 consumers at NextOnEditorActionListener time
                .loadFactor(3)          // 3 jobs per consumer
                .consumerKeepAlive(120) // wait 2 minutes
                .build();

        mJobManager = new JobManager(context, config);
    }

    @Override
    public void execute(final Job job) {
        mJobManager.addJob(new JobWrapper() {
            @Override
            public void onRun() {
                job.onRun();
            }
        });
    }

    @Override
    public void executeDelayed(final Job job, long delay) {
        mJobManager.addJob(new JobWrapper(delay) {
            @Override
            public void onRun() {
                job.onRun();
            }
        });
    }
}