/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import io.buildup.storereview20150911132516.R;

import buildup.ui.BaseListingActivity;

/**
 * RatingsActivity list activity
 */
public class RatingsActivity extends BaseListingActivity {

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.ratingsActivity));
    }

    @Override
    protected Class<? extends Fragment> getFragmentClass() {
        return RatingsFragment.class;
    }
}

