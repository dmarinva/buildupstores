/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ds.restds;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import java.io.ByteArrayOutputStream;
import java.util.Random;

import retrofit.mime.TypedByteArray;

public class ImageUtils {

    private static final Random random = new Random();

    @Nullable
    public static TypedByteArray typedByteArrayFromBitmap(Bitmap bitmap){
        if(bitmap == null)
            return null;

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return new TypedByteArrayWithFilename("image/jpeg", stream.toByteArray(), getTempName());
    }

    private static String getTempName(){
        return "image" + random.nextInt() + ".jpg";
    }
}
