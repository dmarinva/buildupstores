/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.events;

import android.os.Handler;
import android.os.Looper;

import de.greenrobot.event.EventBus;

/**
 * Bus implementation that ensures that all events are posted to Main (UI) thread
 * Based on GreenRobot's EventBus
 */
public class BusProvider extends EventBus implements Bus{

    Handler handler = new Handler(Looper.getMainLooper());

    private static BusProvider mInstance = new BusProvider();

    public static Bus getInstance(){
        return mInstance;
    }

    @Override
    public void post(final Object event) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                BusProvider.super.post(event);
            }
        });
    }
}