/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.events;

/**
 * Bus event
 */
public class ItemEditEvent {

    public int position;

    public Object target;

    public Class tag;

    public ItemEditEvent(int position, Class tag, Object target) {
        this.position = position;
        this.target = target;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return new StringBuilder("ItemEditEvent: ")
                .append("(position = ")
                .append(position)
                .append(", obj = ")
                .append(target.toString())
                .append(")")
                .toString();
    }
}
