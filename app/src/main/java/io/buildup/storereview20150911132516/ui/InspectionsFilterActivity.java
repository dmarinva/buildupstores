/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */


package io.buildup.storereview20150911132516.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import io.buildup.storereview20150911132516.R;

import buildup.ui.BaseFragment;
import buildup.ui.FilterActivity;

import buildup.dialogs.ValuesSelectionDialog;
import buildup.views.ListSelectionPicker;
import io.buildup.storereview20150911132516.ds.StoreDSDS;
import java.util.ArrayList;

/**
 * InspectionsFilterActivity filter activity
 */
public class InspectionsFilterActivity extends FilterActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        // set title
        setTitle(R.string.inspectionsFilterActivity);
    }

    @Override
    protected Fragment getFragment() {
        return new PlaceholderFragment();
    }

    public static class PlaceholderFragment extends BaseFragment {
        // filter field values
            
    ArrayList<String> name_values;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // inflate this fragment's layout
            View rootView = inflater.inflate(R.layout.inspections_filter, container, false);
            return rootView;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            // Get saved values
            Bundle bundle = savedInstanceState;
            if(bundle == null)
                bundle = getArguments();

            // get initial data
                        
            name_values = bundle.getStringArrayList("name_values");

            // bind pickers
                        
            final ListSelectionPicker name_view = (ListSelectionPicker) view.findViewById(R.id.name_filter);
            ValuesSelectionDialog name_dialog = (ValuesSelectionDialog) getFragmentManager().findFragmentByTag("name");
            if (name_dialog == null) 
                name_dialog = new ValuesSelectionDialog();
            
            // configure the dialog
            name_dialog.setColumnName("name")
                .setDatasource(StoreDSDS.getInstance(getActivity().getApplicationContext()))
                .setTitle("name")
                .setHaveSearch(true)
                .setMultipleChoice(true);
            
            // bind the dialog to the picker
            name_view.setSelectionDialog(name_dialog)
                .setTag("name")
                .setSelectedValues(name_values)
                .setSelectedListener(new ListSelectionPicker.ListSelectedListener() {
                @Override
                public void onSelected(ArrayList<String> selected) {
                    name_values = selected;
                }
            });

            // Bind buttons
            Button okBtn = (Button) view.findViewById(R.id.ok);
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();

                    // send filter result back to caller
                                        
                    intent.putStringArrayListExtra("name_values", name_values);

                    getActivity().setResult(RESULT_OK, intent);
                    getActivity().finish();
                }
            });

            Button cancelBtn = (Button) view.findViewById(R.id.reset);
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Reset values
                                        
                    name_values = new ArrayList<String>();
                    name_view.setSelectedValues(null);
                }
            });
        }

        @Override
        public void onSaveInstanceState(Bundle bundle) {
            super.onSaveInstanceState(bundle);

            // save current status
                        
            bundle.putStringArrayList("name_values", name_values);
        }
    }

}
