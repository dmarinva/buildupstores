/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.events;

public interface Executor {
    public void execute(Job job);
    public void executeDelayed(Job job, long delay);
}
