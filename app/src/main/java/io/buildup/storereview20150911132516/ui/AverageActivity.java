/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import io.buildup.storereview20150911132516.R;

import buildup.ui.BaseListingActivity;
import buildup.actions.StartActivityAction;
import buildup.events.ItemSelectedEvent;
import buildup.util.Constants;
import io.buildup.storereview20150911132516.ds.StoreItem;

/**
 * AverageActivity list activity
 */
public class AverageActivity extends BaseListingActivity {

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.averageActivity));
    }

    @Override
    protected Class<? extends Fragment> getFragmentClass() {
        return AverageFragment.class;
    }

    // Event Listener
    public void onEvent(ItemSelectedEvent ev){
        if(ev.target instanceof StoreItem && "AverageFragment".equals(ev.tag)){
            StoreItem item = (StoreItem) ev.target;
            
            Bundle args = new Bundle();
            
            args.putInt(Constants.ITEMPOS, ev.position);
            args.putParcelable(Constants.CONTENT, item);
            navigateToDetail(AverageDetailActivity.class, AverageDetailFragment.class, args);
        }
    }
}

