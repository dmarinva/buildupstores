/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */


package io.buildup.storereview20150911132516.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import buildup.ui.BaseDetailActivity;

/**
 * GoodPerformanceDetailActivity detail activity
 */
public class GoodPerformanceDetailActivity extends BaseDetailActivity {
  
  	@Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected Class<? extends Fragment> getFragmentClass() {
        return GoodPerformanceDetailFragment.class;
    }
}

