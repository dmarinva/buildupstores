/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.presenters;

import io.buildup.storereview20150911132516.R;
import io.buildup.storereview20150911132516.ds.StoreItem;
import io.buildup.storereview20150911132516.interactors.DeleteStoreItemJob;
import io.buildup.storereview20150911132516.interactors.UpdateStoreItemJob;
import io.buildup.storereview20150911132516.interactors.CreateStoreItemJob;
import io.buildup.storereview20150911132516.events.DeleteStoreItemResult;
import io.buildup.storereview20150911132516.events.CreateStoreItemResult;
import io.buildup.storereview20150911132516.events.UpdateStoreItemResult;

import java.util.List;

import buildup.ds.RestService;
import buildup.events.Bus;
import buildup.events.Executor;
import buildup.mvp.BaseFormPresenter;
import buildup.mvp.FormPresenter;
import buildup.mvp.FormView;

public class InspectionsFormPresenter extends BaseFormPresenter<StoreItem> implements FormPresenter<StoreItem> {


    public InspectionsFormPresenter(RestService service, Bus bus, Executor executor, FormView<StoreItem> view){
        super(service, bus, executor, view);
    }

    @Override
    public void deleteItem(StoreItem item) {
        mExecutor.execute(new DeleteStoreItemJob(item, mService, mBus));
    }

    @Override
    public void save(StoreItem item) {
        // validate
        if (validate(item)){
            mExecutor.execute(new UpdateStoreItemJob(item, mService, mBus));
        }
        else
            mView.showMessage(R.string.correct_errors, false);
    }

    @Override
    public void create(StoreItem item) {
        // validate
        if (validate(item)){
            mExecutor.execute(new CreateStoreItemJob(item, mService, mBus));
        }
        else
            mView.showMessage(R.string.correct_errors, false);
    }

    public void onEvent(DeleteStoreItemResult result){
        mView.showMessage(R.string.item_deleted, true);
        mView.close(true);
    }

    public void onEvent(CreateStoreItemResult result){
        mView.setItem(result.item);
        mView.showMessage(R.string.item_created, true);
        mView.close(true);
    }

    public void onEvent(UpdateStoreItemResult result){
        mView.setItem(result.item);
        mView.showMessage(R.string.item_updated, true);
        mView.close(true);
    }
}
