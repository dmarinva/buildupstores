/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import buildup.R;
import buildup.ds.Datasource;
import buildup.ds.Pagination;
import buildup.ds.SearchOptions;
import buildup.ds.filter.Filter;
import buildup.events.BusProvider;
import buildup.events.DatasourceFailureEvent;
import buildup.mvp.DetailView;
import buildup.util.Constants;

/**
 * Fragments to show a detail view inside a {@link android.support.v4.app.Fragment}
 *
 * DetailFragments expect the {@link Constants#CONTENT} to be passed in
 * That argument is optional, given that you implement the {@link #getDatasource()}
 * method.
 */
public abstract class DetailFragment<T>
        extends BaseFragment
        implements Filterable, DetailView {

    int mItemPos;

    Datasource<T> mDatasource;

    T mItem;

    /**
     * Containers for views
     */
    View mContentContainer;

    View mProgressContainer;

    // Search options for filtering
    SearchOptions mSearchOptions;

    public DetailFragment() {
        super();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onCreate(Bundle state) {
        super.onCreate(state);
        Bundle args = getArguments();

        // restore state either from savedState or defaults
        if (state != null) {
            mItem = (T) state.getParcelable(Constants.CONTENT);
        }
        if (mItem == null) {
            mItem = (T) args.getParcelable(Constants.CONTENT);
        }

        mItemPos = args.getInt(Constants.ITEMPOS, 0);
    }

    /**
     * Trick to know when the fragment is being shown (onResume doesn't work for this when the
     * fragment is in the context of a viewpager)
     * This method is highly dependant on
     * {@link android.support.v4.app.FragmentStatePagerAdapter#setPrimaryItem(ViewGroup,
     * int, Object)}
     * but works in all cases.
     * {@inheritDoc}
     */
    @Override
    public void setMenuVisibility(boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && mItem != null) {
            onShow(mItem);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);

        mProgressContainer = view.findViewById(R.id.progressContainer);
        mContentContainer = view.findViewById(R.id.contentContainer);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedState) {
        super.onViewCreated(view, savedState);
        initView(view, savedState);
    }

    /**
     * Init this view for details.
     * @param view
     * @param savedState
     */
    protected void initView(View view, Bundle savedState){
        // we put this here to ensure all onCreate chain has been called,
        // and so getDatasource will return a valid (non null) value
        mDatasource = getDatasource();

        if (mItem != null) {
            bindView(mItem, view);
            setContentShown(true);
        } else {
            refresh();
        }
    }

    /**
     * Updates the item being shown, and perform the appropiate binding
     *
     * @param newItem the new item to show
     */
    public void setItem(T newItem) {
        mItem = newItem;

        View v = getView();
        if (v != null) {   // only bind to view if it already exists
            bindView(mItem, v);
            setContentShown(true);
            onShow(mItem);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save the current item into outState
        outState.putParcelable(Constants.CONTENT, (Parcelable) mItem);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onResume() {
        super.onResume();

        if (mItem != null) {
            onShow(mItem);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mDatasource = null;
        mItem = null;
    }

    // Filterable interface

    protected Datasource.Listener<T> dataListener = new Datasource.Listener<T>() {
        @Override
        public void onSuccess(final T result) {
            mItem = result;
            final View v = getView();
            // force UI thread
            Activity act = getActivity();
            if (act != null) {
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (v != null)   // only bind to view if it already exists
                        {
                            bindView(result, v);
                        }
                        setContentShown(true);
                    }
                });
            }
        }

        @Override
        public void onFailure(Exception e) {
            // force UI thread
            Activity act = getActivity();
            if (act != null) {
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // inform to users
                        BusProvider.getInstance().post(new DatasourceFailureEvent());
                        setContentShown(false);
                    }
                });
            }
        }
    };

    protected Datasource.Listener<List<T>> dataListListener = new Datasource.Listener<List<T>>(){

        @Override
        public void onSuccess(List<T> ts) {
            if(ts.size() > 0)
                dataListener.onSuccess(ts.get(0));
            else
                dataListener.onSuccess(null);
        }

        @Override
        public void onFailure(Exception e) {
            dataListener.onFailure(e);
        }
    };

    public void refresh() {
        if (mDatasource != null) {
            setContentShown(false);
            if(mSearchOptions != null && mDatasource instanceof Pagination){
                // don't use mItemPos and take the first item
                ((Pagination<T> ) mDatasource).getItems(0, 1, mSearchOptions, dataListListener);
            }
            else {
                mDatasource.getItem(String.valueOf(mItemPos), dataListener);
            }
        } else {
            throw new IllegalStateException("Either Item or Datasource should be implemented");
        }
    }

    @Override
    public void setSearchText(String s) {
        ensureSearchOptions();
        mSearchOptions.setSearchText(s);
    }

    @Override
    public void addFixedFilter(Filter filter) {
        ensureSearchOptions();
        mSearchOptions.addFixedFilter(filter);
    }

    @Override
    public void addFilter(Filter filter) {
        ensureSearchOptions();
        mSearchOptions.addFilter(filter);
    }

    private void ensureSearchOptions(){
        if (mSearchOptions == null)
            mSearchOptions = new SearchOptions();
    }

    @Override
    public void showMessage(int message, boolean toast){
        if(toast)
            Toast.makeText(getActivity(), getString(message), Toast.LENGTH_SHORT).show();
        else
            Snackbar.make(getView(), getString(message), Snackbar.LENGTH_LONG).show();
    }

    protected void setContentShown(boolean shown) {
        if (mProgressContainer != null && mContentContainer != null) {
            mProgressContainer.setVisibility(shown ? View.GONE : View.VISIBLE);
            mContentContainer.setVisibility(shown ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void navigateToEditForm() {
        // do nothing. implemented in subclasses
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // if the item is updated, we refresh the view
        if(requestCode == Constants.MODE_EDIT && resultCode == Constants.CONTENT_UPDATED){

            T item = (T) data.getParcelableExtra(Constants.CONTENT);
            if(item != null)
                setItem(item);

            // set our result code (in phone mode, we have our own activity)
            getActivity().setResult(resultCode, data);
        }
    }

    @Override
     public void close(boolean shouldRefresh) {
        // close detail activity if we are in phone mode
        // or a navigation activity is not the fragment owner
        Activity act = getActivity();

        if(!getResources().getBoolean(R.bool.tabletLayout) ||
                act instanceof BaseDetailActivity){
            // TODO: Support tablet layouts
            Intent data = new Intent();
            data.putExtra(Constants.CONTENT, (Parcelable) (shouldRefresh ? null:getItem()));
            act.setResult(Constants.CONTENT_UPDATED, data);
            act.finish();
        }
    }

    /**
     * Gets the current item
     *
     * @return the current item, or null if it's not been retrieved yet
     */
    public T getItem() {
        return mItem;
    }

    /**
     * called when a view is ready to be binded to data, but maybe it's not
     * showing yet (i.e. in a {@link android.support.v4.view.ViewPager})
     *
     * @param item an instance of T data
     * @param view the view to bind data to
     */
    public abstract void bindView(T item, View view);

    /**
     * This method will return a datasource for data retrieval
     *
     * @return a datasource
     */
    public Datasource<T> getDatasource() {
        return null;
    }

    /**
     * Called whenever a fragment is shown to the user
     *
     * @param item the item being shown
     */
    protected void onShow(T item){};

    /**
     * The layout for this fragment
     * @return the layout id for this screen
     */
    protected abstract int getLayout();

}
