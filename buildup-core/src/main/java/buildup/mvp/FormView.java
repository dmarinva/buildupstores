/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.mvp;

/**
 * View (as in MVP) for forms
 */
public interface FormView<T> {
    /**
     * Show a message to user
     * @param toast if the message should be persistent (toast)
     * @param messageId
     */
    public void showMessage(int messageId, boolean toast);

    /**
     * Close the form and return to the caller
     * @param shouldRefresh if the caller should refresh (usually a listview)
     */
    public void close(boolean shouldRefresh);

    /**
     * sets the item to edit
     * @param item
     */
    public void setItem(T item);
}
