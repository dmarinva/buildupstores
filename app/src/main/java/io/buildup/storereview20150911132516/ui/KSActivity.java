/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import io.buildup.storereview20150911132516.MyApplication;
import io.buildup.storereview20150911132516.R;
import io.buildup.storereview20150911132516.presenters.KSPresenter;

import buildup.events.BusProvider;
import buildup.events.ExecutorProvider;
import buildup.ui.BaseActivity;

public class KSActivity extends BaseActivity implements KSPresenter.KSView {

    private static final String FIRST_OPEN = "FIRST_OPEN";
    private TextView mSplashMessage;
    private KSPresenter mPresenter;
    private long mFirstOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        mSplashMessage = (TextView) findViewById(R.id.splash_message);

        mFirstOpen = getSharedPreferences().getLong(FIRST_OPEN, System.currentTimeMillis());
        mPresenter = new KSPresenter(BusProvider.getInstance(),
                ExecutorProvider.getInstance(getApplicationContext()),
                this,
                mFirstOpen,
                getResources().getInteger(R.integer.splash_seconds) * 1000,
                getResources().getInteger(R.integer.killswitch_hours) * 60 * 60 * 1000);

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    protected void initView(){
        mSplashMessage.setText(R.string.splash_message);
    }

    public SharedPreferences getSharedPreferences() {
        return ((MyApplication) getApplication()).getSecureSharedPreferences();
    }

    // KSView interface

    @Override
    public void startApp(){
        getSharedPreferences().edit().putLong(FIRST_OPEN, mFirstOpen).apply();
        Intent intent = new Intent(this, DavidTestMain.class);
        startActivity(intent);
    }

    @Override
    public void showAppExpired() {
        mSplashMessage.setText(R.string.killswitch_expired);
    }
}

