/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.actions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.lang.ref.WeakReference;

import buildup.util.Constants;

public class StartActivityAction implements Action {

    Class mClazz;

    WeakReference<Context> mWeakContext;
    private int mRequestCode;

    public StartActivityAction(Context context, Class clazz){
        this(context, clazz, 0);
    }

    public StartActivityAction(Context context, Class clazz, int requestCode) {
        this.mClazz = clazz;
        this.mWeakContext = new WeakReference<Context>(context);
        mRequestCode = requestCode;
    }

    @Override
    public void execute(Bundle runtimeArgs) {
        Context context = mWeakContext.get();
        if (context != null) {
            Intent intent = new Intent(context, this.mClazz);
            if (runtimeArgs != null) {
                intent.putExtras(runtimeArgs);
            }

            if(mRequestCode != 0)
                ((Activity) context).startActivityForResult(intent, mRequestCode);
            else
                context.startActivity(intent);
        }
    }
}
