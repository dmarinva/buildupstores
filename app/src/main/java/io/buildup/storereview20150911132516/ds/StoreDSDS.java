/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */


package io.buildup.storereview20150911132516.ds;
 
import android.content.Context;

import java.net.URL;
import java.util.List;
import buildup.ds.SearchOptions;
import buildup.ds.restds.AppNowDatasource;
import buildup.util.StringUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * "StoreDSDS" data source. (e37eb8dc-6eb2-4635-8592-5eb9696050e3)
 */
public class StoreDSDS extends AppNowDatasource<StoreItem>{

    // default page size
    static final int PAGE_SIZE = 20;

    static StoreDSDS instance;

    private StoreService mService;

    public static StoreDSDS getInstance(Context context){
        if(instance == null)
            instance = new StoreDSDS(context);

        return instance;
    }

    private StoreDSDS(Context context) {
        this.mService = StoreService.getInstance(context);
    }

    @Override
    public void getItem(String id, final Listener<StoreItem> listener) {
        // query first item
        if (id.equalsIgnoreCase(("0")))
            getItems(new Listener<List<StoreItem>>() {
                @Override
                public void onSuccess(List<StoreItem> items) {
                    if(items != null && items.size() > 0)
                        listener.onSuccess(items.get(0));
                    else
                        listener.onSuccess(new StoreItem());
                }

                @Override
                public void onFailure(Exception e) {
                    listener.onFailure(e);
                }
            });
        else
            mService.getServiceProxy().getStoreItemById(id, new Callback<StoreItem>() {
                @Override
                public void success(StoreItem result, Response response) {
                    listener.onSuccess(result);
                }

                @Override
                public void failure(RetrofitError error) {
                    listener.onFailure(error);
                }
            });       

    }

    @Override
    public void getItems(final Listener<List<StoreItem>> listener) {
        getItems(null, listener);
    }


    @Override
    public void getItems(SearchOptions options, Listener<List<StoreItem>> listener) {
        getItems(0, 0, options, listener);
    }

    @Override
    public void getItems(int pagenum, int pagesize, SearchOptions options, final Listener<List<StoreItem>> listener) {
        String conditions = getConditions(options, getSearchableFields());
        int skipNum = pagenum * pagesize;
        String skip = skipNum == 0 ? null : String.valueOf(skipNum);
        String limit = pagesize == 0 ? null: String.valueOf(pagesize);
        String sort = getSort(options);

        mService.getServiceProxy().queryStoreItem(
                skip,
                limit,
                conditions,
                sort,
                null,
                null,
                new Callback<StoreItem.List>() {
            @Override
            public void success(StoreItem.List result, Response response) {
                listener.onSuccess(result);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onFailure(error);
            }
        });
    }

    private String[] getSearchableFields() {
        return new String[]{"name", "lastReview", "review", "picture"};
    }

    // Pagination

    @Override
    public int getPageSize(){
        return StoreDSDS.PAGE_SIZE;
    }

    @Override
    public void getUniqueValuesFor(String searchStr, final Listener<List<String>> listener) {
        mService.getServiceProxy().distinct(searchStr, new Callback<List<String>>() {
            @Override
            public void success(List<String> result, Response response) {
                listener.onSuccess(result);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onFailure(error);
            }
        });
    }

    @Override
    public URL getImageUrl(String path) {
        return mService.getImageUrl(path);
    }
}
