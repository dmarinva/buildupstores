/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.mvp;

public interface Presenter {
    public void onResume();
    public void onPause();
}
