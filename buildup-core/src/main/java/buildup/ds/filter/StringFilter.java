package buildup.ds.filter;

import android.text.TextUtils;

/**
 * Created by jiparis on 15/09/2015.
 */
public class StringFilter implements Filter<String> {

    private final String mValue;
    private final String mField;

    public StringFilter(String field, String value){
        mField = field;
        mValue = value;
    }

    @Override
    public String getField() {
        return mField;
    }

    @Override
    public String getQueryString() {
        return "\"" + mField + "\":{\"$regex\":\"" + mValue + "\",\"$options\":\"i\"}";
    }

    @Override
    public boolean applyFilter(String fieldValue) {
        return (mValue == null) || mValue.toLowerCase().contains(mValue.toLowerCase());
    }
}
