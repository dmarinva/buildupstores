/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ui;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import buildup.behaviors.FABBehavior;
import buildup.behaviors.ShareBehavior;
import buildup.ds.restds.AppNowDatasource;
import buildup.events.BusProvider;
import buildup.events.ExecutorProvider;
import buildup.mvp.DetailCrudPresenter;
import buildup.util.ColorUtils;
import buildup.util.Constants;
import buildup.util.ImageLoader;
import buildup.util.StringUtils;
import io.buildup.storereview20150911132516.ds.StoreService;
import io.buildup.storereview20150911132516.presenters.InspectionsDetailPresenter;
import io.buildup.storereview20150911132516.R;
import java.net.URL;
import buildup.ds.Datasource;
import io.buildup.storereview20150911132516.ds.StoreDSDS;
import io.buildup.storereview20150911132516.ds.StoreItem;

public class InspectionsDetailFragment extends buildup.ui.DetailFragment<StoreItem> implements ShareBehavior.ShareListener {

    public static InspectionsDetailFragment newInstance(Bundle args){
        InspectionsDetailFragment fr = new InspectionsDetailFragment();
        fr.setArguments(args);

        return fr;
    }

    public InspectionsDetailFragment(){
        super();
    }      

    @Override
    public Datasource<StoreItem> getDatasource(){
        return StoreDSDS.getInstance(getActivity().getApplication());
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        // the presenter for this view
        setPresenter(new InspectionsDetailPresenter(
                StoreService.getInstance(getActivity().getApplication()),
                BusProvider.getInstance(),
                ExecutorProvider.getInstance(getActivity().getApplication()),
                this));
        // Edit button
        addBehavior(new FABBehavior(this, R.drawable.ic_edit_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DetailCrudPresenter<StoreItem>) getPresenter()).editForm(getItem());
            }
        }));
        addBehavior(new ShareBehavior(getActivity(), this));
    }

    // Bindings

    @Override
    protected int getLayout() {
        return R.layout.inspectionsdetail_detail;
    }

    @Override
    @SuppressLint("WrongViewCast")
    public void bindView(final StoreItem item, View view) {
        
        ImageView view0 = (ImageView) view.findViewById(R.id.view0);
        URL view0Media = ((AppNowDatasource) getDatasource()).getImageUrl(item.picture);
        if(view0Media != null){
        	ImageLoader.loadWithSpinner(getActivity(),
        			view0Media.toExternalForm(), view,
        			view0,"view0");
        	
        }
        if (item.name != null){
            
            TextView view1 = (TextView) view.findViewById(R.id.view1); 
            view1.setText(item.name);
            
        }
        if (item.reviewDate != null){
            
            TextView view2 = (TextView) view.findViewById(R.id.view2); 
            view2.setText(DateFormat.getMediumDateFormat(getActivity()).format(item.reviewDate));
            
        }
        if (item.review != null){
            
            TextView view3 = (TextView) view.findViewById(R.id.view3); 
            view3.setText("\"" + item.review + "\"");
            
        }
        if (item.sales != null){
            
            TextView view4 = (TextView) view.findViewById(R.id.view4); 
            view4.setText(item.sales.toString());
            
        }
        if (item.performance != null){
            
            TextView view5 = (TextView) view.findViewById(R.id.view5); 
            view5.setText(item.performance.toString());
            
        }
    }

    @Override
    protected void onShow(StoreItem item) {
        // set the title for this fragment
        getActivity().setTitle(null);
    }

    @Override
    public void navigateToEditForm() {
        Bundle args = new Bundle();

        args.putInt(Constants.ITEMPOS, 0);
        args.putParcelable(Constants.CONTENT, getItem());
        args.putInt(Constants.MODE, Constants.MODE_EDIT);

        Intent intent = new Intent(getActivity(), StoreItemFormActivity.class);
        intent.putExtras(args);
        startActivityForResult(intent, Constants.MODE_EDIT);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.delete_menu, menu);

        MenuItem item = menu.findItem(R.id.action_delete);
        ColorUtils.tintIcon(item, R.color.textBarColor, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_delete){
            ((DetailCrudPresenter<StoreItem>) getPresenter()).deleteItem(getItem());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
        
    @Override
    public void onShare() {
        StoreItem item = getItem();
           
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");

        intent.putExtra(Intent.EXTRA_TEXT, (item.name != null ? item.name : "" ) + "\n" +
                    (item.reviewDate != null ? DateFormat.getMediumDateFormat(getActivity()).format(item.reviewDate) : "" ) + "\n" +
                    (item.review != null ? "\"" + item.review + "\"" : "" ) + "\n" +
                    (item.sales != null ? item.sales.toString() : "" ) + "\n" +
                    (item.performance != null ? item.performance.toString() : "" ));
        startActivityForResult(Intent.createChooser(intent,
                        getString(R.string.share)), 1);
    }    
}
