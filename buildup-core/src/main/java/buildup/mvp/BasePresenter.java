/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.mvp;

import buildup.events.Bus;

public abstract class BasePresenter implements Presenter{

    protected final Bus mBus;

    public BasePresenter(Bus bus){
        mBus = bus;
    }

    public void onResume(){
        mBus.register(this);
    }

    public void onPause(){
        mBus.unregister(this);
    }
}
