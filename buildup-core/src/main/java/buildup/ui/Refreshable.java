/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ui;

/**
 * UI component (activity or fragment) that supports refresh operations
 */
public interface Refreshable {

    /**
     * Refresh this view
     */
    public void refresh();
}
