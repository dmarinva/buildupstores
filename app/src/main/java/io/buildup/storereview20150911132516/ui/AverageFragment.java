/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */
package io.buildup.storereview20150911132516.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import buildup.ds.Datasource;
import buildup.ds.filter.NumberFilter;
import buildup.ui.ListGridFragment;
import buildup.util.ViewHolder;
import io.buildup.storereview20150911132516.R;
import io.buildup.storereview20150911132516.ds.StoreDSDS;
import io.buildup.storereview20150911132516.ds.StoreItem;
/**
 * "AverageFragment" listing
 */
public class AverageFragment extends ListGridFragment<StoreItem> {

    public AverageFragment(){
        super();
    }

    public static AverageFragment newInstance(Bundle args){
        AverageFragment fr = new AverageFragment();

        fr.setArguments(args);
        return fr;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addFixedFilter(new NumberFilter("performance", 2));
    }

    // ListGridFragment interface

    /**
    * Layout for the list itselft
    */
    @Override
    protected int getLayout() {
        return R.layout.fragment_list;
    }

    /**
    * Layout for each element in the list
    */
    @Override
    protected int getItemLayout() {
        return R.layout.average_item;
    }

    @Override
    protected Datasource<StoreItem> getDatasource(){
        return StoreDSDS.getInstance(getActivity().getApplication());
    }

    @Override
    protected void bindView(StoreItem item, View view, int position) {
        
        TextView title = ViewHolder.get(view, R.id.title);
        
        if (item.name != null){
            title.setText(item.name);
            
        }
        
        TextView subtitle = ViewHolder.get(view, R.id.subtitle);
        
        if (item.review != null){
            subtitle.setText("\"" + item.review + "\"");
            
        }
        
    }

}
