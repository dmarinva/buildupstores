/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.services;

/**
 * Base interface for a login service
 */
public interface LoginService {

    public void attemptLogin(String email, String password);
}
