/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.mvp;

/**
 * View (as in MVP) for listings
 */
public interface ListView<T> {
    public void refresh();
    public void showMessage(int messageRes);
    public void navigateToDetail(T item);
}
