/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.mvp;

import java.util.List;

public interface DetailCrudPresenter<T> extends Presenter{
    /**
     * delete item
     * @param item
     */
    void deleteItem(T item);

    /**
     * Go to edit form
     * @param item
     */
    void editForm(T item);

}
