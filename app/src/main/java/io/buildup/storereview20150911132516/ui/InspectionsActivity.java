/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import io.buildup.storereview20150911132516.R;

import buildup.ui.BaseListingActivity;
import android.os.Parcelable;
import buildup.actions.StartActivityAction;
import buildup.events.ItemEditEvent;
import buildup.events.ItemSelectedEvent;
import buildup.util.Constants;
import io.buildup.storereview20150911132516.ds.StoreItem;

/**
 * InspectionsActivity list activity
 */
public class InspectionsActivity extends BaseListingActivity {

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.inspectionsActivity));
    }

    @Override
    protected Class<? extends Fragment> getFragmentClass() {
        return InspectionsFragment.class;
    }

    // Event Listener
    public void onEvent(ItemSelectedEvent ev){
        if(ev.target instanceof StoreItem && "InspectionsFragment".equals(ev.tag)){
            StoreItem item = (StoreItem) ev.target;
            
            Bundle args = new Bundle();
            
            args.putInt(Constants.ITEMPOS, ev.position);
            args.putParcelable(Constants.CONTENT, item);
            navigateToDetail(InspectionsDetailActivity.class, InspectionsDetailFragment.class, args);
        }
    }
    public void onEvent(ItemEditEvent ev){
        
        if(ev.tag.equals(StoreItem.class)){
            Bundle args = new Bundle();
        
            args.putInt(Constants.ITEMPOS, ev.position);
            args.putParcelable(Constants.CONTENT, (Parcelable) ev.target);
            args.putInt(Constants.MODE, ev.target == null ? Constants.MODE_CREATE : Constants.MODE_EDIT);
            navigateToDetail(StoreItemFormActivity.class, StoreItemFormFragment.class, args);
        }
        
    }
}

