/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import buildup.R;
import buildup.dialogs.ImagePickerOptionsDialog;
import buildup.util.ImageLoader;

public class ImagePicker extends LinearLayout implements View.OnClickListener, ImagePickerOptionsDialog.OnOptionSelectedListener {
    public static final int GALLERY_REQUEST_CODE = 1024;
    public static final int CAPTURE_REQUEST_CODE = 2048;
    public static final String FILE_PICKER_TAG = "FilePicker";

    private String mLabel;
    private TextView mErrorView;
    private TextView mLabelView;
    private ImageView mImageView;
    private ImagePickerOptionsDialog mImagePickerDialog;
    private FragmentManager mFragmentManager;
    private Fragment mFragment;
    private int mIndex;
    boolean mHasImage;
    private Callback mCallback;

    public ImagePicker(Context context) {
        this(context, null);
    }

    public ImagePicker(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ImagePicker, 0, 0);
        mLabel = a.getString(R.styleable.ImagePicker_label);

        mIndex = a.getInt(R.styleable.ImagePicker_index, 0);

        a.recycle();

        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.file_picker, this, true);

        mErrorView = (TextView) findViewById(R.id.error);
        mLabelView = (TextView) findViewById(R.id.label);
        mLabelView.setText(mLabel);

        mImageView = (ImageView) findViewById(R.id.image);

        mLabelView.setOnClickListener(this);
        mImageView.setOnClickListener(this);

        // Try to restore previous suspended fragment
        mFragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
        mImagePickerDialog = (ImagePickerOptionsDialog) mFragmentManager.findFragmentByTag(FILE_PICKER_TAG);
        if(mImagePickerDialog == null) {
            mImagePickerDialog = new ImagePickerOptionsDialog();
        }
    }

    public void setImageBitmap(Bitmap bm){
        mHasImage = true;
        this.mImageView.setImageBitmap(bm);
    }

    public void setImageUrl(@NonNull String url){
        // may produce null pointer
        mHasImage = true;
        ImageLoader.load(getContext(), url, mImageView);
    }

    public void setTargetFragment(Fragment fr){
        mFragment = fr;
    }

    public void clear(){
        mHasImage = false;
        ImageLoader.load(R.drawable.ic_image_photo, mImageView);
    }

    @Override
    public void onClick(View v) {
        if(mHasImage)
            mImagePickerDialog.setRemoveEnabled(true);
        mImagePickerDialog.setListener(this);
        mImagePickerDialog.show(mFragmentManager, FILE_PICKER_TAG);
    }

    // OnOptionSelectedListener int

    @Override
    public void fromStorage() {
        // Choose from folder
        Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        fileIntent.setType("image/*");

        // picked image will be received in the container fragment
        mFragment.startActivityForResult(
                Intent.createChooser(fileIntent, null),
                GALLERY_REQUEST_CODE + mIndex);
    }

    @Override
    public void fromCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // picked image will be received in the container fragment
        mFragment.startActivityForResult(
                Intent.createChooser(cameraIntent, null),
                CAPTURE_REQUEST_CODE + mIndex
        );
    }

    @Override
    public void remove() {
        if(mCallback != null)
            mCallback.imageRemoved();
    }

    public void setCallback(Callback callback){
        mCallback = callback;
    }

    public Callback getCallback(){
        return mCallback;
    }

    public void setError(int errorRes){
        setError(getContext().getString(errorRes));
    }

    public void setError(CharSequence errorMsg) {
        if (errorMsg != null) {
            if (mErrorView.getVisibility() == GONE) {
                mErrorView.setText(errorMsg);
                mErrorView.setAlpha(0.0F);
                mErrorView.setVisibility(VISIBLE);
                mErrorView.animate()
                        .alpha(1.0F)
                        .setDuration(200L)
                        .setInterpolator(new FastOutSlowInInterpolator())
                        .setListener(null).start();
            }
        } else {
            mErrorView.animate()
                    .alpha(0.0F)
                    .setDuration(200L)
                    .setInterpolator(new FastOutSlowInInterpolator())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mErrorView.setText((CharSequence) null);
                            mErrorView.setVisibility(GONE);
                        }
                    });
        }
    }

    public interface Callback{
        void imageRemoved();
    }
}
