/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.ds;
import android.content.Context;
import java.net.URL;
import io.buildup.storereview20150911132516.R;
import buildup.ds.RestService;
import buildup.util.StringUtils;

/**
 * "StoreService" REST Service implementation
 */
public class StoreService extends RestService<StoreServiceRest>{

    private static StoreService instance;
    private Context mContext;

    public static StoreService getInstance(Context context){
        if(instance == null)
            instance = new StoreService(context);

        return instance;
    }

    private StoreService(Context context) {
        super(StoreServiceRest.class);
        this.mContext = context;
    }

    @Override
    public String getServerUrl() {
        return "https://pod-sweet-cies-5775.herokuapp.com";
    }

    @Override
    protected String getApiKey() {
        return "icinetic";
    }

    @Override
    public URL getImageUrl(String path){
        return StringUtils.parseUrl("https://pod-sweet-cies-5775.herokuapp.com",
                path,
                "apikey=icinetic");
    }

}
