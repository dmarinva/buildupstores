/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import buildup.R;
import buildup.adapters.DatasourceAdapter;
import buildup.behaviors.Behavior;
import buildup.behaviors.SwipeRefreshBehavior;
import buildup.ds.Datasource;
import buildup.ds.Pagination;
import buildup.ds.filter.DateRangeFilter;
import buildup.ds.filter.Filter;
import buildup.ds.filter.StringListFilter;

import buildup.events.BusProvider;
import buildup.events.ItemSelectedEvent;
import buildup.mvp.ListCrudPresenter;
import buildup.util.Constants;
import buildup.util.EndlessScrollListener;

/**
 * A fragment representing a list of Items that come from a datasource. Subclasses must
 * implement the {@link ListGridFragment#getDatasource()}, and
 * {@link #bindView(Object, View, int)} methods.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView, dinamically.
 * <p/>
 */
public abstract class ListGridFragment<T>
        extends BaseFragment
        implements Refreshable, Filterable, buildup.mvp.ListView<T>,
        AbsListView.OnItemClickListener, AbsListView.OnItemLongClickListener {

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private DatasourceAdapter<T> mAdapter;

    private Datasource<T> mDatasource;

    // set to true when the fragment is created the first time (this fragment is sticky)
    private boolean mJustCreated;

    /**
     * Containers for views
     */
    View mListContainer;

    View mProgressContainer;

    /**
     * Loading more... item
     */
    View mFooter;

    /**
     * List header
     */
    int mHeaderRes;
    View mHeaderView;

    EndlessScrollListener mScrollListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListGridFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // lists are refreshable
        addBehavior(new SwipeRefreshBehavior(this));

        // we set this fragments sticky to cache list contents on configuration changes
        setRetainInstance(true);
        mJustCreated = true;

        // create datasource and adapter
        mDatasource = getDatasource();
        // adapter instantation
        mAdapter = createAdapter();
        mAdapter.setCallback(new DatasourceAdapter.Callback() {
            @Override
            public void onDataAvailable() {
                setListShown(true);

                // remove footer view
                if (mFooter != null) {
                    ((ListView) mListView).removeFooterView(mFooter);
                }

                // inform the scroll listener so that it continues processing scrolls
                if (mScrollListener != null) {
                    mScrollListener.finishLoading();
                }
            }

            @Override
            public void onPageRequested() {
                // add loading ... view
                if (mFooter != null) {
                    ((ListView) mListView).addFooterView(mFooter);
                }
            }

            @Override
            public void onDatasourceError(Exception e){
                Toast.makeText(getActivity(), R.string.error_data_generic, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);

        // Set up the listview
        mListView = (AbsListView) view.findViewById(android.R.id.list);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);

        // set up pagination
        if (mDatasource instanceof Pagination) {
            mScrollListener = new EndlessScrollListener() {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    mAdapter.loadNextPage();
                }
            };

            mListView.setOnScrollListener(mScrollListener);

            // set the footer loading more... item
            // this must be done BEFORE assigning the adapter (in KitKat this is fixed)
            if (mListView instanceof ListView) {
                mFooter = inflater.inflate(R.layout.list_footer, null, false);
                ((ListView) mListView).addFooterView(mFooter);
            }
        }

        // register header
        if(mListView instanceof ListView && mHeaderRes != 0) {
            mHeaderView = inflater.inflate(mHeaderRes, null, false);
            ((ListView) mListView).addHeaderView(mHeaderView, null, false);
        }

        // register adapter (after any header or footer)
        mListView.setAdapter(mAdapter);

        // again, remove the footer from the list
        if (mDatasource instanceof Pagination && mListView instanceof ListView) {
            ((ListView) mListView).removeFooterView(mFooter);
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListContainer = view.findViewById(R.id.listContainer);
        mProgressContainer = view.findViewById(R.id.progressContainer);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        // load first batch of data
        if (mJustCreated) {
            refresh();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // set the created flag off (this fragment is retained)
        mJustCreated = false;
    }

    @Override
    public void onDestroy() {
        // this actually is not called, since this fragment is retained
        mDatasource = null;
        mAdapter = null;

        super.onDestroy();
    }

    protected DatasourceAdapter<T> createAdapter() {
        // create datasource and get layout and binding
        return new DatasourceAdapter<T>(
                getActivity(),
                getItemLayout(),
                mDatasource) {

            @Override
            public void bindView(T item, int position, View view) {
                ListGridFragment.this.bindView(item, view, position);
            }
        };
    }

    private void setListShown(boolean shown) {
        if (mProgressContainer != null && mListContainer != null) {
            mProgressContainer.setVisibility(shown ? View.GONE : View.VISIBLE);
            mListContainer.setVisibility(shown ? View.VISIBLE : View.GONE);
        }
    }

    public AbsListView getListView(){
        return mListView;
    }

    public void setHeaderResource(int resource){
        this.mHeaderRes = resource;
    }

    public View getHeaderView(){
        return mHeaderView;
    }

    // Click listeners
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        itemClicked((T) parent.getAdapter().getItem(position), position);
    }

    // Default click implementation (without presenter)
    protected void itemClicked(T item, int position){
        this.navigateToDetail(item);
    };

    @Override
     public void navigateToDetail(T item) {
        // by default, navigation is implemented in activities (main or list activity)
        // with this message, we notify activities
        BusProvider.getInstance()
                .post(new ItemSelectedEvent(
                        0,
                        this.getClass().getSimpleName(),
                        item));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // if we did launch an activity for editing, this will refresh on return
        if(resultCode == Constants.CONTENT_UPDATED){
            refresh();
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id){
        boolean res = false;
        if(mBehaviors != null){
            for (Behavior b: mBehaviors){
                res = b.onItemLongClick(parent, view, position, id) || res;
            }
        }
        return res;
    }

    // Refreshable interface

    public void refresh() {
        setListShown(false);
        mListView.clearChoices();
        mAdapter.refresh();
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyText instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    // from ListView (MVP)

    @Override
    public void showMessage(int messageRes){
        Snackbar.make(getView(), getString(messageRes), Snackbar.LENGTH_LONG).show();
    }

    /**
     * get the adapter this fragment is attached to
     *
     * @return the adapter
     */
    public DatasourceAdapter<T> getAdapter() {
        return mAdapter;
    }

    // Search methods

    /**
     * set the filtering criteria for the adapter. This criteria belongs of the page
     */
    public void setSearchText(String searchText) {
        mAdapter.setSearchText(searchText);
    }

    // Sorting

    /**
     * set the sorting column for the adapter.
     */
    public void setSortColumn(String sortColumn) {
        mAdapter.setSortColumn(sortColumn);
    }

    /**
     * set the comparator object that will be used to sort the local data
     */
    public void setSortComparator(Comparator c) {
        mAdapter.setSortComparator(c);
    }

    /**
     * set the sorting order.
     */
    public void setSortAscending(boolean value) {
        mAdapter.setSortAscending(value);
    }

    // Filtering

    @Override
    public void addFilter(Filter filter){
        mAdapter.addFilter(filter);
    }

    @Override
    public void addFixedFilter(Filter filter) {
        mAdapter.addFixedFilter(filter);
    }

    /**
     * @see DatasourceAdapter#addFilter(buildup.ds.filter.Filter)
     */
    public void addStringFilter(String field, List<String> values) {
        mAdapter.addFilter(new StringListFilter(field, values));
    }

    public void addDateRangeFilter(String field, long value1, long value2) {
        Date min = (value1 != -1) ? new Date(value1) : null;
        Date max = (value2 != -1) ? new Date(value2) : null;
        mAdapter.addFilter(new DateRangeFilter(field, min, max));
    }

    public void resetFilters() {
        mAdapter.resetFilters();
    }

    // Presenter

    @Override
    public ListCrudPresenter<T> getPresenter() {
        return (ListCrudPresenter<T>) super.getPresenter();
    }


    // Delegates

    /**
     * Get the datasource for this list
     * @return a @link Datasource object
     */
    protected abstract Datasource<T> getDatasource();

    /**
     * Get the layout for this fragment.
     * @return a valid layout for lists (fragment_list, fragment_grid3cols and fragment_grid4cols)
     */
    protected abstract int getLayout();

    /**
     * Get the layout for this list's items
     * @return the item layout
     */
    protected abstract int getItemLayout();

    /**
     * Binds the item layout to each list item.
     * @param item The item to bind to the view
     * @param view The view inflated from #getItemLayout()
     * @param position the current position
     */
    protected abstract void bindView(T item, View view, int position);


}
