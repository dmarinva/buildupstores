/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ui;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import buildup.R;
import buildup.events.BusProvider;
import buildup.events.DatasourceFailureEvent;
import buildup.events.DatasourceUnauthorizedEvent;

/**
 * Base activity for all buildup activities
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    // set up generic event listeners
    public void onEvent(DatasourceFailureEvent event) {
        Snackbar.make(getRootView(), R.string.error_data_generic,
                Snackbar.LENGTH_SHORT).show();
    }

    public void onEvent(DatasourceUnauthorizedEvent event) {
        Snackbar.make(getRootView(), R.string.error_data_unauthorized,
                Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private View getRootView(){
        return ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
    }
}
