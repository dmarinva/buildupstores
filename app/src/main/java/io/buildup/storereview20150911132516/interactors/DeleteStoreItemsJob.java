/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.interactors;
import io.buildup.storereview20150911132516.ds.StoreServiceRest;
import io.buildup.storereview20150911132516.ds.StoreItem;
import io.buildup.storereview20150911132516.events.DeleteStoreItemResult;

import java.util.ArrayList;
import java.util.List;

import buildup.ds.RestService;
import buildup.events.Bus;
import buildup.events.DatasourceFailureEvent;
import buildup.events.Job;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * StoreItem Multiple Delete Job
 */
public class DeleteStoreItemsJob implements Job{
    private final List<StoreItem> mItems;
    private final RestService<StoreServiceRest> mService;
    private final Bus mBus;

    public DeleteStoreItemsJob(List<StoreItem> item,
                         RestService<StoreServiceRest> service,
                         Bus bus){
        super();
        mService = service;
        mItems = item;
        mBus = bus;
    }

    @Override
    public void onRun() {
        mService.getServiceProxy().deleteByIds(collectIds(), new Callback<List<StoreItem>>() {
            @Override
            public void success(List<StoreItem> item, Response response) {
                mBus.post(new DeleteStoreItemResult());
            }

            @Override
            public void failure(RetrofitError error) {
                mBus.post(new DatasourceFailureEvent());
            }
        });
    }

    protected List<String> collectIds(){
        ArrayList<String> ids = new ArrayList<>();
        for(StoreItem item: mItems){
            ids.add(item.id);
        }

        return ids;
    }
}
