/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */
package io.buildup.storereview20150911132516.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import buildup.behaviors.FABBehavior;
import buildup.behaviors.SearchBehavior;
import buildup.behaviors.SelectionBehavior;
import buildup.ds.Datasource;
import buildup.events.BusProvider;
import buildup.events.ExecutorProvider;
import buildup.ui.ListGridFragment;
import buildup.util.ColorUtils;
import buildup.util.Constants;
import buildup.util.ViewHolder;
import io.buildup.storereview20150911132516.ds.StoreService;
import io.buildup.storereview20150911132516.presenters.InspectionsPresenter;
import io.buildup.storereview20150911132516.R;
import java.util.ArrayList;
import java.util.List;
import io.buildup.storereview20150911132516.ds.StoreDSDS;
import io.buildup.storereview20150911132516.ds.StoreItem;
/**
 * "InspectionsFragment" listing
 */
public class InspectionsFragment extends ListGridFragment<StoreItem> {
    
    ArrayList<String> name_values;
    // "Add" button
    FABBehavior mFABBehavior;

    public InspectionsFragment(){
        super();
    }

    public static InspectionsFragment newInstance(Bundle args){
        InspectionsFragment fr = new InspectionsFragment();

        fr.setArguments(args);
        return fr;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPresenter(new InspectionsPresenter(
            StoreService.getInstance(getActivity().getApplication()),
            BusProvider.getInstance(),
            ExecutorProvider.getInstance(getActivity().getApplication()),
            this
        ));
        addBehavior(new SearchBehavior(this));
        // Multiple selection
        SelectionBehavior<StoreItem> selectionBehavior = new SelectionBehavior<>(
            this,
            R.string.remove_items,
            R.drawable.ic_delete_alpha);

        selectionBehavior.setCallback(new SelectionBehavior.Callback<StoreItem>() {
            @Override
            public void onSelected(List<StoreItem> selectedItems) {
                getPresenter().deleteItems(selectedItems);
            }
        });
        addBehavior(selectionBehavior);
        // FAB button
        mFABBehavior = new FABBehavior(this, R.drawable.ic_add_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().addForm();
            }
        });
        addBehavior(mFABBehavior);
    }

    // ListGridFragment interface

    /**
    * Layout for the list itselft
    */
    @Override
    protected int getLayout() {
        return R.layout.fragment_list;
    }

    /**
    * Layout for each element in the list
    */
    @Override
    protected int getItemLayout() {
        return R.layout.inspections_item;
    }

    @Override
    protected Datasource<StoreItem> getDatasource(){
        return StoreDSDS.getInstance(getActivity().getApplication());
    }

    @Override
    protected void bindView(StoreItem item, View view, int position) {
        
        TextView title = ViewHolder.get(view, R.id.title);
        
        if (item.name != null){
            title.setText(item.name);
            
        }
        
        TextView subtitle = ViewHolder.get(view, R.id.subtitle);
        
        if (item.reviewDate != null){
            subtitle.setText(DateFormat.getMediumDateFormat(getActivity()).format(item.reviewDate));
            
        }
        
    }

    @Override
    protected void itemClicked(final StoreItem item, int position) {
        mFABBehavior.hide(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                getPresenter().detail(item);
            }
        });
    }

    @Override
    public void navigateToDetail(StoreItem item) {
        // we launch the detail activity only in case we are not in tablet mode
        // in that case, our activity will do the job
        if(!getResources().getBoolean(R.bool.tabletLayout)){
            Bundle args = new Bundle();

            args.putParcelable(Constants.CONTENT, item);
            Intent intent = new Intent(getActivity(), InspectionsDetailActivity.class);
            intent.putExtras(args);
            startActivityForResult(intent, Constants.DETAIL);
        }
        else{
            super.navigateToDetail(item);
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // inflate menu options and tint icon
        inflater.inflate(R.menu.filter_menu, menu);
        ColorUtils.tintIcon(menu.findItem(R.id.filter),
                            R.color.textBarColor,
                            getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.filter){
            Intent intent = new Intent(getActivity(), InspectionsFilterActivity.class);

            // pass current values to filter activity
                        
            intent.putStringArrayListExtra("name_values", name_values);

            // launch filter screen
            startActivityForResult(intent, 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            // store the incoming selection
                        
            name_values = data.getStringArrayListExtra("name_values");
            // apply filter to datasource
            resetFilters();
            
                        
            if(name_values != null && name_values.size() > 0)
                addStringFilter("name", name_values);
            // and finally refresh the list
            refresh();

            // and redraw menu (to refresh tinted icons, like the search icon)
            getActivity().invalidateOptionsMenu();
        }
    }
}
