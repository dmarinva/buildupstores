/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

/*
 * Created on 2015/10/12
 * Android Formula 3.0.22 - Radarc 4.7.3.23583 
 */


package io.buildup.storereview20150911132516;

import android.app.Application;
import android.app.Application;
import android.app.Activity;
import android.os.Bundle;
import buildup.util.SecurePreferences;
import android.content.Intent;
import io.buildup.storereview20150911132516.ui.KSActivity;

/**
 * You can use this as a global place to keep application-level resources
 * such as singletons, services, etc. 
 */
public class MyApplication extends Application implements Application.ActivityLifecycleCallbacks {
    private SecurePreferences mSharedPreferences;
    private boolean mKSShown = false;

    @Override
    public void onCreate() {
        super.onCreate();
        mSharedPreferences = new SecurePreferences(this);
        registerActivityLifecycleCallbacks(this);

  
    }

    public SecurePreferences getSecureSharedPreferences() {
        return mSharedPreferences;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) { 
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (activity instanceof KSActivity)
            return;
        else {
            // Kill switch
            showKillSwitchIfNeeded(activity);
        }

 
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {        
    }

    @Override
    public void onActivityStopped(Activity activity) {
 
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    /**
     * show kill switch if needed
     * @param activity the current activity
     * @return true if ks is going to be shown
     */
    private boolean showKillSwitchIfNeeded(Activity activity){
        if(mKSShown)
            return false;
        
        Intent ksIntent = new Intent(this, KSActivity.class);
        ksIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_NO_HISTORY);

        startActivity(ksIntent);
        mKSShown = true;

        // remove previous activity
        activity.finish();

        return true;
    }
}
