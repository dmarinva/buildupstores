/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.util;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.lang.reflect.Method;

/**
 * Utils for fragment instantiation
 */
public class FragmentUtils {

    // fragment instantiation
    public static Fragment instantiate(Class<? extends Fragment> clazz, Bundle defaults) {
        try {
            Method method = clazz.getMethod("newInstance", Bundle.class);
            return (Fragment) method.invoke(null, defaults);
        } catch (Exception e) {
            Log.d("buildup", "Exception instantiating the fragment [" + clazz.getName() + "]");
            throw new IllegalArgumentException("Couldn't instantie fragment: + clazz.getName()", e);
        }
    }


}
