/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ds;

/**
 * This interface mark the datasource as cacheable
 */
public interface Cache {

    public void invalidate();
}
