/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */


package io.buildup.storereview20150911132516.ui;

import android.os.Bundle;

import io.buildup.storereview20150911132516.R;

import java.util.ArrayList;
import java.util.List;

import buildup.MenuItem;

import buildup.actions.StartActivityAction;
import buildup.util.Constants;

/**
 * RatingsFragment menu fragment.
 */
public class RatingsFragment extends buildup.ui.MenuFragment {

    /**
     * Default constructor
     */
    public RatingsFragment(){
        super();
    }

    // Factory method
    public static RatingsFragment newInstance(Bundle args) {
        RatingsFragment fragment = new RatingsFragment();

        fragment.setArguments(args);
        return fragment;
    }

    // Menu Fragment interface

    @Override
    public List<MenuItem> getMenuItems() {
        ArrayList<MenuItem> items = new ArrayList<MenuItem>();
        items.add(new MenuItem()
            .setLabel("5★ to 4★")
            .setIcon(R.drawable.png_iconstar5png166)
            .setAction(new StartActivityAction(getActivity(), GoodPerformanceActivity.class, Constants.DETAIL))
        );
        items.add(new MenuItem()
            .setLabel("3★")
            .setIcon(R.drawable.png_iconstar3png818)
            .setAction(new StartActivityAction(getActivity(), AverageActivity.class, Constants.DETAIL))
        );
        items.add(new MenuItem()
            .setLabel("2★ to ★")
            .setIcon(R.drawable.png_iconstar2png116)
            .setAction(new StartActivityAction(getActivity(), BadPerformanceActivity.class, Constants.DETAIL))
        );
        return items;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_grid;
    }

    @Override
    public int getItemLayout() {
        return R.layout.ratings_item;
    }
}
