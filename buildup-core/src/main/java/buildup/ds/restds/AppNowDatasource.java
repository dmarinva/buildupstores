/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.ds.restds;

import android.text.TextUtils;

import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import buildup.ds.Datasource;
import buildup.ds.Distinct;
import buildup.ds.Pagination;
import buildup.ds.SearchOptions;
import buildup.ds.filter.Filter;

/**
 * AppNow Datasource
 */
public abstract class AppNowDatasource<T> implements Datasource<T>, Pagination<T>, Distinct {

    // Utility methods
    protected String getConditions(SearchOptions options, String[] searchCols){
        if(options == null)
            return null;

        // Filters
        ArrayList<String> exps = new ArrayList<>();

        addFilters(exps, options.getFilters(), false);
        addFilters(exps, options.getFixedFilters(), true);

        // TODO: support full text search with $text
        String st = options.getSearchText();
        if (st != null && searchCols != null && searchCols.length > 0){
            ArrayList<String> searches = new ArrayList<>();
            for(String col: searchCols){
                searches.add("{\"" + col + "\":{\"$regex\":\"" + st + "\",\"$options\":\"i\"}}");
            }
            String searchExp = "\"$or\":[" + TextUtils.join(",", searches) + "]";
            exps.add(searchExp);
        }

        if (exps.size() > 0)
            return "{" + TextUtils.join(",", exps) + "}";

        return null;
    }

    private void addFilters(ArrayList<String> exps, List<Filter> filters, boolean fixed){

        if(filters != null){
            List<String> filterExps = new ArrayList<>();

            for(Filter filter: filters){
                String qs = filter.getQueryString();
                if (qs != null)
                    filterExps.add(qs);
            }

            if(filterExps.size() > 0) {
                if(fixed)
                    exps.add("\"$and\":[{" + TextUtils.join("},{", filterExps) + "}]");
                else
                    exps.addAll(filterExps);
            }
        }

    }

    protected String getSort(SearchOptions options){
        if(options == null)
            return null;

        String col = options.getSortColumn();
        boolean asc = options.isSortAscending();

        if(col == null)
            return null;

        if (!asc)
            col = "-" + col;

        return col;
    }

    // search (without pagination)
    public abstract void getItems(SearchOptions options, Listener<List<T>> listener);

    /**
     * Get the url for a image resource in this datasource
     * @param path the image path (can be relative or absolute)
     * @return the URL object you can pass to an ImageLoader class
     */
    public abstract URL getImageUrl(String path);
}
