/*
 * Copyright (c) 2015.
 * This code is part of Buildup (http://www.buildup.io)
 */

package buildup.events;

public interface Bus {
    void post(Object event);
    void register(Object observer);
    void unregister(Object observer);
}
