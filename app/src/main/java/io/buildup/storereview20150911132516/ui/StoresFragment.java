/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */
package io.buildup.storereview20150911132516.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import buildup.behaviors.SearchBehavior;
import buildup.ds.Datasource;
import buildup.ds.restds.GeoPoint;
import buildup.util.Constants;
import io.buildup.storereview20150911132516.ds.StoreDSDS;
import io.buildup.storereview20150911132516.ds.StoreItem;

/**
 * "StoresFragment" listing
 */
public class StoresFragment extends buildup.maps.ui.MapFragment<StoreItem> {
    private static final int UPDATE_STORE_REQUEST_CODE = 12334;
    private SearchBehavior<StoreItem> searchBehavior;
    public static StoresFragment newInstance(Bundle args){
        StoresFragment fr = new StoresFragment();
        fr.setArguments(args);

        return fr;
    }

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If searchable behavior
        setHasOptionsMenu(true);
        searchBehavior = new SearchBehavior<>(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        searchBehavior.onCreateOptionsMenu(menu, inflater);
    }
	@Override
    protected Datasource getDatasource() {
        return StoreDSDS.getInstance(getActivity().getApplicationContext());
    }

    @Override
    protected int getMapType() {
        return GoogleMap.MAP_TYPE_TERRAIN;
    }

    @Override
    protected String getLocationField() {
        return "location";
    }

    @Override
    protected Marker createAndBindMarker(GoogleMap map, StoreItem item) {
        return map.addMarker(new MarkerOptions()
                        .position(
                                new LatLng(getLocationForItem(item).coordinates[1],
                                        getLocationForItem(item).coordinates[0]))
                        // Binding
                        .title(item.name)
                        .snippet("$ " + item.sales.toString())
        );
    }


    protected GeoPoint getLocationForItem(StoreItem item) {        
        return item.location;
    }

    @Override
    public void navigateToDetail(StoreItem item) {
        Intent intent = new Intent(getActivity(), StoreItemFormActivity.class);
        intent.putExtra(Constants.CONTENT, item);
        intent.putExtra(Constants.MODE, Constants.MODE_EDIT);

        startActivityForResult(intent, UPDATE_STORE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (UPDATE_STORE_REQUEST_CODE == requestCode && resultCode == 100) {
            refresh();
        }
    }
}
