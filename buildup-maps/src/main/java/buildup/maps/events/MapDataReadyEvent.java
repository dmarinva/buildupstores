package buildup.maps.events;

import java.util.List;

public class MapDataReadyEvent<T> {

    public final List<T> data;

    public MapDataReadyEvent(List<T> data){
        this.data = data;
    }
}
