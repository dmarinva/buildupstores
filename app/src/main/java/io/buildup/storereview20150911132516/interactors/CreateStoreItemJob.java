/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.interactors;
import io.buildup.storereview20150911132516.ds.StoreServiceRest;
import io.buildup.storereview20150911132516.ds.StoreItem;
import io.buildup.storereview20150911132516.events.CreateStoreItemResult;

import buildup.ds.restds.ImageUtils;
import buildup.ds.RestService;
import buildup.events.Bus;
import buildup.events.DatasourceFailureEvent;
import buildup.events.Job;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * StoreItem Create Job
 */
public class CreateStoreItemJob implements Job{
 	private final StoreItem mItem;
    private final RestService<StoreServiceRest> mService;
    private final Bus mBus;

    public CreateStoreItemJob(StoreItem item,
                         RestService<StoreServiceRest> service,
                         Bus bus){
        super();
        mService = service;
        mItem = item;
        mBus = bus;
    }

    @Override
    public void onRun() {
        
        if(mItem.pictureBitmap != null){
            mService.getServiceProxy().createStoreItem(mItem,
                ImageUtils.typedByteArrayFromBitmap(mItem.pictureBitmap),
                callback);
        }
        else
            mService.getServiceProxy().createStoreItem(mItem, callback);
        
    }

    Callback<StoreItem> callback = new Callback<StoreItem>() {
        @Override
        public void success(StoreItem item, Response response) {
            mBus.post(new CreateStoreItemResult(item));
        }

        @Override
        public void failure(RetrofitError error) {
            mBus.post(new DatasourceFailureEvent());
        }
    };
}
