/*
 * This App has been generated using http://buildup.io , the Bright Enterprise App Builder.
 */

package io.buildup.storereview20150911132516.events;

import io.buildup.storereview20150911132516.ds.StoreItem;

/**
 * StoreItem Item created event
 */
public class CreateStoreItemResult{
	public StoreItem item;

	public CreateStoreItemResult(StoreItem result){
		this.item = result;
	}
}
